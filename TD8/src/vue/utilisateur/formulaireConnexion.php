<?php
use App\GenerateurAvis\Modele\DataObject\Utilisateur;
/** @var Utilisateur $utilisateur */
?>

<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="controleur" value="utilisateur"/>
    <input type="hidden" name="action" value="connecter"/>
    <fieldset>
        <legend>Connexion :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" name="login" id="login_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Se connecter"/>
        </p>
    </fieldset>
</form>
