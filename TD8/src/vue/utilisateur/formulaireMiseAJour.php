<?php
use App\GenerateurAvis\Modele\DataObject\Utilisateur;
/** @var Utilisateur $utilisateur */

?>
<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="controleur" value="utilisateur"/>
    <input type="hidden" name="action" value="mettreAJour"/>

    <fieldset>
        <legend>Modifiaction d'utilisateur :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text"  name="login" id="login_id" value="<?= htmlspecialchars($utilisateur->getLogin())?>" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text"  name="nom" id="nom_id" value="<?= htmlspecialchars($utilisateur->getNom())?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
            <input class="InputAddOn-field" type="text" name="prenom" id="prenom_id" value="<?= htmlspecialchars($utilisateur->getPrenom())?>" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="ancien_mdp_id">Ancien mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" name="ancien_mdp" id="ancien_mdp_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nouveau_mdp_id">Nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" name="nouveau_mdp" id="nouveau_mdp_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="confirmation_mdp_id">Confirmer le nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" name="confirmation_mdp" id="confirmation_mdp_id" required>
        </p>

        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>

    </fieldset>
</form>