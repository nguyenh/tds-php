<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php /**
         * @var string $titre
         */
        echo $titre; ?></title>
    <link rel="stylesheet" href="../ressources/css/navstyle.css">
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?controleur=utilisateur&action=afficherListe">Gestion des
                    utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?controleur=trajet&action=afficherListe">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference">
                    <img src="../ressources/img/heart.png" alt="Heart Icon" style="width:20px;height:20px;">
                </a>
            </li>
            <?php

            use App\Covoiturage\Lib\ConnexionUtilisateur;

            if (!ConnexionUtilisateur::estConnecte()) { ?>
                <li>
                    <a href="controleurFrontal.php?action=afficherFormulaireCreation">
                        <img src="../ressources/img/add-user.png" alt="Add user">
                    </a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherFormulaireConnexion">
                        <img src="../ressources/img/enter.png" alt="Connect">
                    </a>
                </li>

            <?php } else { ?>
                <li>
                    <a href="controleurFrontal.php?action=afficherDetail&login=<?php echo ConnexionUtilisateur::getLoginUtilisateurConnecte(); ?>">
                        <img src="../ressources/img/user.png" alt="Information of user"">
                    </a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=deconnecter">
                        <img src="../ressources/img/logout.png" alt="Logout">
                    </a>
                </li>
                <?php
                if (ConnexionUtilisateur::estAdministrateur()) { ?>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulaireCreation">
                            <img src="../ressources/img/add-user.png" alt="Add user">
                        </a>
                    </li>
                <?php }
            }
            ?>
        </ul>
    </nav>

</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Hong Hoa
    </p>
</footer>
</body>
</html>

