<?php
use App\Covoiturage\Lib\PreferenceControleur;
?>

<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="enregistrerPreference"/>
    <fieldset>
        <legend>Contrôleur par défaut :</legend>

        <p class="InputAddOn">
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
                <?= (PreferenceControleur::lire() === 'utilisateur') ? 'checked' : '' ?>>
            <label for="utilisateurId">Utilisateur</label>
        </p>

        <p class="InputAddOn">
            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
                <?= (PreferenceControleur::lire() === 'trajet') ? 'checked' : '' ?>>
            <label for="trajetId">Trajet</label>
        </p>

        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
