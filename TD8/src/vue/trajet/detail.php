<?php
/** @var \App\Covoiturage\Modele\DataObject\Trajet[] $trajet */

use App\Covoiturage\Modele\DataObject\Utilisateur;



if ($trajet->isNonFumeur()) {
    echo '<p> Le trajet numero ' . htmlspecialchars($trajet->getId()) . " part de  " . htmlspecialchars($trajet->getDepart()) . ' et va jusqu\'à  ' . htmlspecialchars($trajet->getArrivee()) . ' le ' . $trajet->getDate()->format("Y-m-d ") . '. Il coute ' . htmlspecialchars($trajet->getPrix()) . '€ et il s\'agit d\'un trajet non fumeur. C\'est ' . htmlspecialchars($trajet->getConducteur()->getLogin()) . ' qui conduit </p>';
}
else {
    echo '<p> Le trajet numero ' . htmlspecialchars($trajet->getId()) . " part de  " . htmlspecialchars($trajet->getDepart()) . ' et va jusqu\'à  ' . htmlspecialchars($trajet->getArrivee()) . ' le ' . $trajet->getDate()->format("Y-m-d ") . '. Il coute ' . htmlspecialchars($trajet->getPrix()) . '€ et il s\'agit d\'un trajet autorisé aux  fumeurs. C\'est ' . htmlspecialchars($trajet->getConducteur()->getLogin()) . ' qui conduit </p>';

}


    ?>