<?php
namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;

    private string $mdpHache;

    // un getter

    /**
     * @param string $login
     * @param string $nom
     * @param string $prenom
     * @param string $mdpHache
     */
    public function __construct(string $login, string $nom, string $prenom, string $mdpHache)
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = $mdpHache;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }


    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    // un setter
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    // un setter
    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }



    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString() :string {
        return "L'utilisateur $this->nom $this->prenom, de login $this->login";
    }*/

}

?>