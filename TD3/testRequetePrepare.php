<?php

use Modele\Utilisateur;

require_once 'ModeleUtilisateur.php';

// Crée un nouvel utilisateur
$nouvelUtilisateur = new Utilisateur("jdoe", "Doe", "John");

// Ajoute l'utilisateur dans la base de données
$nouvelUtilisateur->ajouter();

echo "ModeleUtilisateur ajouté avec succès.";
?>
