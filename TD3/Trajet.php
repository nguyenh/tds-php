<?php

use Modele\ConnexionBaseDeDonnees;
use Modele\Utilisateur;

require_once 'ConnexionBaseDeDonnees.php';
require_once 'ModeleUtilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;

    /**
     * @var ModeleUtilisateur[]
     */
    private array $passagers;

    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = array();
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            Utilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"]
        );
        $trajet -> setPassagers($trajet->recupererPassagers());

        return $trajet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de $this->depart pour aller à $this->arrivee (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).  
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public static function recupererTrajetParId(int $idTrajet) : ?Trajet {
        $sql = "SELECT * from trajet WHERE id = :idTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTag" => $idTrajet,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $trajetFormatTableau = $pdoStatement->fetch();
        if (!$trajetFormatTableau)
            return null;

        return Trajet::construireDepuisTableauSQL($trajetFormatTableau);
    }

    public function ajouter() : bool {
        $sql = 'INSERT INTO trajet (id, depart, arrivee, date, prix, conducteurLogin, nonFumeur) VALUES (:idTag, :departTag, :arriveeTag, :dateTag, :prixTag, :conducteurLoginTag, :nonFumeurTag);';
        $pdoStatement = ConnexionBaseDeDonnees::getPDO() -> prepare($sql);

        $values = array(
            'idTag' => $this->id,
            'departTag' => $this->depart,
            'arriveeTag' => $this->arrivee,
            'dateTag' => $this->date->format("Y-m-d"),
            'prixTag' => $this->prix,
            'conducteurLoginTag' => $this->conducteur->getLogin()
        );
        switch ($this->nonFumeur) {
            case true: $values["nonFumeurTag"] = 1;
                break;
            default: $values["nonFumeurTag"] = 0;
                break;
        }

        $pdoStatement -> execute($values);
        if (ConnexionBaseDeDonnees::getPdo()->lastInsertId() == $this->id) {
            return true;
        }
        return false;
    }

    /**
     * @return ModeleUtilisateur[]
     */
    private function recupererPassagers() : array {
        $sql = 'SELECT p.passagerLogin
                FROM passager p
                WHERE p.trajetId = :trajetIdTag;';

        $pdoStatement = ConnexionBaseDeDonnees::getPdo() -> prepare($sql);

        $values = array(
            'trajetIdTag' => $this->id
        );

        $pdoStatement -> execute($values);

        $listeUtilisateur = $pdoStatement -> fetchAll(PDO::FETCH_ASSOC);

        $Utilisateur = array();
        foreach($listeUtilisateur as $utilisateurFormatted) {
            $Utilisateur[] = Utilisateur::recupererUtilisateurParLogin($utilisateurFormatted['passagerLogin']);
        }

        return $Utilisateur;
    }

    public function supprimerPassager(string $passagerLogin): bool {
        $sql = 'DELETE FROM passager WHERE passagerLogin = :passagerLogin AND trajetId = :trajetIdTag;';
        $pdoStatement = ConnexionBaseDeDonnees::getPdo() -> prepare($sql);

        $values = array(
            'passagerLogin' => $passagerLogin,
            'trajetIdTag' => $this->id
        );

        $nombreLigne = $pdoStatement->rowCount();
        $pdoStatement -> execute($values);

        if ($nombreLigne != $pdoStatement->rowCount()) {
            return true;
        }
        return false;
    }
}