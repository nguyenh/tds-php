<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Test de ma classe Utilisateur.php </title>
</head>

<body>
<p>
    <?php

    use Modele\ConnexionBaseDeDonnees;
    use Modele\Utilisateur;

    require_once 'ModeleUtilisateur.php';
    $user = new Utilisateur($_POST["login"], $_POST["nom"], $_POST["prenom"]);
    $user->ajouter();
    echo ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM utilisateur')->fetch();
    ?>
</p>
</body>
</html>