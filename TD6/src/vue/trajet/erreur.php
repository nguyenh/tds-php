<h1>Problème avec le trajet</h1>
<p>Une erreur est survenue. Veuillez réessayer plus tard ou contacter le support.</p>
<?php if (isset($message)): ?>
    <p><?= htmlspecialchars($message) ?></p>
<?php else: ?>
    <p>Le trajet demandé n'existe pas ou le id n'a pas été fourni.</p>
<?php endif; ?>
<!-- <a href="controleurFrontal.php?controleur=trajet&action=afficherListe">Retour à la liste des trajets</a> -->