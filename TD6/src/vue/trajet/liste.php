<h1>Liste des trajets</h1>
<ul>
    <?php

    use App\Covoiturage\Modele\DataObject\Trajet;

    /** @var Trajet[] $trajets */

    foreach ($trajets as $trajet):
        $idHTML = htmlspecialchars($trajet->getId());
        $departHTML = htmlspecialchars($trajet->getDepart());
        $arriveeHTML = htmlspecialchars($trajet->getArrivee());
        $dateHTML = htmlspecialchars($trajet->getDate()->format('Y-m-d'));
        $prixHTML = htmlspecialchars($trajet->getPrix());

        echo '
        <li>
            <p> Trajet n°<a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=' . $idHTML . '">' . $idHTML . '</a> : De ' . $departHTML . ' à ' . $arriveeHTML . ' le ' . $dateHTML . ' pour ' . $prixHTML . '€ (
            <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=' . $idHTML . '">Modifier ?</a>,
            <a href="controleurFrontal.php?controleur=trajet&action=supprimer&id=' . $idHTML . '">Supprimer ?</a>)</p> 
        </li>';
    endforeach; ?>
</ul>
<!-- Lien vers le formulaire de création de trajet -->
<a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation">Créer un trajet</a>
