<?php

use App\Covoiturage\Modele\DataObject\Trajet;

/** @var Trajet $trajet */
?>
<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="mettreAJour"/>
    <input type='hidden' name='controleur' value='trajet'>
    <input type="hidden" name="id" value="<?= htmlspecialchars($trajet->getId() ?? '') ?>"/>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Départ&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Montpellier" name="depart" id="depart_id"
                   value="<?= htmlspecialchars($trajet->getDepart()) ?>" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Séte" name="arrivee" id="arrivee_id"
                   value="<?= htmlspecialchars($trajet->getArrivee()) ?>" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" placeholder="Ex : JJ/MM/AAAA" name="date" id="date_id"
                   value="<?= htmlspecialchars($trajet->getDate()->format('Y-m-d')) ?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" placeholder="Ex : 20" name="prix" id="prix_id"
                   value="<?= htmlspecialchars($trajet->getPrix()) ?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login du contucteur&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="conducteurLogin"
                   id="conducteurLogin_id" value="<?= htmlspecialchars($trajet->getConducteur()->getLogin()) ?>"
                   required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non fumeur?&#42;</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="Ex : leblancj" name="nonFumeur"
                   id="nonFumeur_id" <?= htmlspecialchars($trajet->isNonFumeur() ? 'checked' : '') ?> >
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>