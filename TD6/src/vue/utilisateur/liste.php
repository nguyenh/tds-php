<h1>Liste des utilisateurs</h1>
<ul>
    <?php

    use App\Covoiturage\Modele\DataObject\Utilisateur;

    /** @var Utilisateur[] $utilisateurs */


    foreach ($utilisateurs as $utilisateur):
        $loginHTML = htmlspecialchars($utilisateur->getLogin());
        $loginURL = rawurlencode($utilisateur->getLogin());
        echo '
        <li><p> Utilisateur de login <a href="controleurFrontal.php?action=afficherDetail&login=' . $loginURL . '">' .
            $loginHTML . '</a> (<a
                        href="controleurFrontal.php?action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Modifier
                    ?</a>, <a href="controleurFrontal.php?action=supprimer&login=' . $loginURL . '">Supprimer ?</a>)</p>
        </li>';
    endforeach; ?>
</ul>
<!-- Lien vers le formulaire de création d'utilisateur -->
<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un utilisateur</a>

