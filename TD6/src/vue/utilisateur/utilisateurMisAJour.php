<?php

use App\Covoiturage\Modele\Repository\UtilisateurRepository;

?>
    <p>L'utilisateur de login <?= /** @var String $login */
    htmlspecialchars($login) ?> a bien été mis à jour</p>

<?php
$utilisateurs = (new UtilisateurRepository())->recuperer();
require __DIR__ . '/liste.php';
?>