<?php if (isset($utilisateur)): ?>
    <p>Le <strong>login </strong> de l'utilisateur <?= htmlspecialchars($utilisateur->getNom()) ?> <?= htmlspecialchars($utilisateur->getPrenom()) ?> est <?= htmlspecialchars($utilisateur->getLogin()) ?></p>


<?php else: ?>
    <p>Pas d'information pour utilisateur.</p>
<?php endif; ?>
