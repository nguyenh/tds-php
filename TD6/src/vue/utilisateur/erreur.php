<h1>Problème avec l’utilisateur</h1>
<?php if (isset($message)): ?>
    <p><?= htmlspecialchars($message) ?></p>
<?php else: ?>
    <p>L'utilisateur demandé n'existe pas ou le login n'a pas été fourni.</p>
<?php endif; ?>
<a href="controleurFrontal.php?action=afficherListe">Retour à la liste des utilisateurs</a>

