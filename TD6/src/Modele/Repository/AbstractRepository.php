<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository
{
    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire(): string;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau): AbstractDataObject;

    /** @return string[] */
    protected abstract function getNomsColonnes(): array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    public function recuperer(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable());

        $tableauObjets = [];
        foreach ($pdoStatement as $objetFormatTableau) {
            $tableauObjets[] = $this->construireDepuisTableauSQL($objetFormatTableau);
        }
        return $tableauObjets;
    }

    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :clePrimaireTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "clePrimaireTag" => $clePrimaire
        );
        $pdoStatement->execute($values);
        $objetFormatTableau = $pdoStatement->fetch();
        if (!$objetFormatTableau)
            return null;

        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    public function supprimer(string $valeurClePrimaire): bool
    {
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " =:clePrimaireTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "clePrimaireTag" => $valeurClePrimaire
        );
        return $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $nomsColonnes = $this->getNomsColonnes();
        $nomTable = $this->getNomTable();

        $colonnes = join(",", $nomsColonnes);
        $tags = join(", ", array_map(fn($colonne) => ':' . $colonne . 'Tag', $nomsColonnes));

        $sql = "INSERT INTO $nomTable ($colonnes) VALUES ($tags);";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);

        //var_dump($values);
        return $pdoStatement->execute($values);
    }

    public function mettreAJour(AbstractDataObject $objet): void
    {
        $nomsColonnes = $this->getNomsColonnes();
        $nomTable = $this->getNomTable();
        $nomClePrimaire = $this->getNomClePrimaire();

        $setClause = join(", ", array_map(fn($colonne) => "$colonne = :$colonne" . 'Tag', $nomsColonnes));
        $sql = "UPDATE $nomTable SET $setClause WHERE $nomClePrimaire = :$nomClePrimaire" . 'Tag;';

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);

        $values["$nomClePrimaire" . 'Tag'] = $objet->{"get" . ucfirst($nomClePrimaire)}();
        $pdoStatement->execute($values);
    }
}