<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use DateMalformedStringException;
use DateTime;

class TrajetRepository extends AbstractRepository
{
    // Méthode pour construire un Trajet depuis un tableau SQL
    /**
     * @throws DateMalformedStringException
     */
    protected function construireDepuisTableauSQL(array $trajetTableau): Trajet
    {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // Transformation de la date string en DateTime
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]), // Récupération de l'objet Utilisateur via le login
            (bool)$trajetTableau["nonFumeur"] // Conversion automatique de l'entier en booléen
        );

        // Récupérer les passagers pour ce trajet
        $trajet->setPassagers(self::recupererPassagers($trajet));

        return $trajet;
    }

    /**
     * @return Utilisateur[]
     */
    static public function recupererPassagers(Trajet $trajet): array
    {
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $sql = "SELECT utilisateur.login, utilisateur.nom, utilisateur.prenom 
                FROM passager
                INNER JOIN utilisateur ON passager.passagerLogin = utilisateur.login
                WHERE passager.trajetId = :trajetId";

        $stmt = $pdo->prepare($sql);
        $stmt->execute([':trajetId' => $trajet->getId()]);

        $passagers = [];
        while ($row = $stmt->fetch()) {
            $passagers[] = (new UtilisateurRepository)->construireDepuisTableauSQL($row);
        }

        return $passagers;
    }

    // Méthode pour récupérer tous les trajets

    /**
     * @return Trajet[]
     * @throws DateMalformedStringException
     */
    /*public static function recupererTrajets(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = (new TrajetRepository)->construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }*/
    protected function getNomTable(): string
    {
        return 'trajet';
    }

    protected function getNomClePrimaire(): string
    {
        return 'id';
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format('Y-m-d'),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->isNonFumeur()
        );
    }

}