<?php
namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): void
    {
        $this->login = substr($login, 0, 64);
    }

    public function getTrajetsCommePassager(): ?array
    {
        if (is_null($this->trajetsCommePassager)) {
            $this->trajetsCommePassager = UtilisateurRepository::recupererTrajetsCommePassager($this);
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


    // un constructeur
    public function __construct(
        $login,
        $nom,
        $prenom
    )
    {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString(): string
    {
        return "Utilisateur : " . $this->prenom . " " . $this->nom . ", login : " . $this->login;
    }*/
}

?>

