<?php

namespace App\Covoiturage\Controleur;


use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur
{
// Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); // appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Liste des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/liste.php',
            'utilisateurs' => $utilisateurs
        ]);
    }

    public static function afficherDetail(): void
    {
        if (!isset($_GET['login'])) {
            self::afficherErreur('Le login de l’utilisateur n’a pas été fourni.');
            return;
        }

        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (!$utilisateur) {
            self::afficherErreur('L’utilisateur demandé n’existe pas.');
            return;
        }
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Details des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/detail.php',
            'utilisateur' => $utilisateur
        ]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Formulaire pour creation des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/formulaireCreation.php'
        ]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue;
    }

    public static function creerDepuisFormulaire(): void
    {
        if (isset($_GET['login'], $_GET['nom'], $_GET['prenom'])) {
            $utilisateur = self::construireDepuisFormulaire($_GET);

            try {
                $success = (new UtilisateurRepository)->ajouter($utilisateur);

                if ($success) {
                    $utilisateurs = (new UtilisateurRepository())->recuperer();
                    self::afficherVue('vueGenerale.php', [
                        'titre' => 'Utilisateur créé avec succès',
                        'cheminCorpsVue' => 'utilisateur/utilisateurCree.php',
                        'utilisateurs' => $utilisateurs
                    ]);
                } else {
                    self::afficherErreur('Erreur lors de la création de l’utilisateur.');
                }
            } catch (\PDOException $e) {
                error_log($e->getMessage());

                self::afficherErreur('L’utilisateur existe déjà. Veuillez utiliser un autre login.');
            }
        } else {
            self::afficherErreur('Tous les champs sont requis.');
        }
    }


    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Erreur',
            'cheminCorpsVue' => 'utilisateur/erreur.php',
            'message' => $messageErreur ? "Problème avec l’utilisateur : $messageErreur" : "Problème avec l’utilisateur"
        ]);
    }

    public static function supprimer(): void
    {
        if (!isset($_GET['login'])) {
            self::afficherErreur('Le login de l’utilisateur n’a pas été fourni.');
            return;
        }

        $login = $_GET['login'];
        $success = (new UtilisateurRepository())->supprimer($login);

        if ($success) {
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Utilisateur supprimé avec succès',
                'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php',
                'login' => $login,
                'utilisateurs' => $utilisateurs
            ]);
        } else {
            self::afficherErreur('Erreur lors de la suppression de l’utilisateur.');
        }
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        if (!isset($_GET['login'])) {
            self::afficherErreur("Le login de l'utilisateur n'a pas été fourni");
            return;
        }

        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (!$utilisateur) {
            self::afficherErreur("L'utilisateur demandé n'existe pas.");
            return;
        }

        self::afficherVue('vueGenerale.php', [
            'titre' => 'Formulaire de mise à jour utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php',
            'utilisateur' => $utilisateur
        ]);
    }

    public static function mettreAJour(): void
    {
        if (isset($_GET['login'], $_GET['nom'], $_GET['prenom'])) {
            $utilisateur = self::construireDepuisFormulaire($_GET);
            (new UtilisateurRepository())->mettreAJour($utilisateur);

            self::afficherVue('vueGenerale.php', [
                'titre' => 'Utilisateur mis à jour avec succès',
                'cheminCorpsVue' => 'utilisateur/utilisateurMisAJour.php',
                'login' => $utilisateur->getLogin(),
                'utilisateur' => $utilisateur
            ]);
        } else {
            self::afficherErreur('Tous les champs sont requis pour la mise à jour.');
        }
    }
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        if (empty($login) || empty($nom) || empty($prenom)) {
            self::afficherErreur('Tous les champs du formulaire sont requis.');
            exit;
        }

        return new Utilisateur($login, $nom, $prenom);
    }
}

?>