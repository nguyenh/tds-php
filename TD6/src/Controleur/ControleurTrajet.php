<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateMalformedStringException;
use DateTime;

class ControleurTrajet
{
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer();; // appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Liste des trajets',
            'cheminCorpsVue' => 'trajet/liste.php',
            'trajets' => $trajets
        ]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue;
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Erreur',
            'cheminCorpsVue' => 'trajet/erreur.php',
            'message' => $messageErreur ? "Problème avec le trajet : $messageErreur" : "Problème avec l’trajet"
        ]);
    }

    public static function afficherDetail(): void
    {
        if (!isset($_GET['id'])) {
            self::afficherErreur('Le id du trajet n’a pas été fourni.');
            return;
        }

        $login = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($login);

        if (!$trajet) {
            self::afficherErreur('Le trajet demandé n’existe pas.');
            return;
        }
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Details des trajets',
            'cheminCorpsVue' => 'trajet/detail.php',
            'trajet' => $trajet
        ]);
    }
    public static function supprimer(): void
    {
        if (!isset($_GET['id'])) {
            self::afficherErreur('Le id du trajet  n’a pas été fourni.');
            return;
        }

        $id = $_GET['id'];
        $success = (new TrajetRepository())->supprimer($id);

        if ($success) {
            $trajets = (new TrajetRepository())->recuperer();
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Trajet supprimé avec succès',
                'cheminCorpsVue' => 'trajet/trajetSupprime.php',
                'id' => $id,
                'trajets' => $trajets
            ]);
        } else {
            self::afficherErreur('Erreur lors de la suppression du trajet.');
        }
    }
    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Formulaire pour creation des trajets',
            'cheminCorpsVue' => 'trajet/formulaireCreation.php'
        ]);
    }

    /**
     * @throws DateMalformedStringException
     */
    public static function creerDepuisFormulaire(): void
    {
        if (isset($_GET['depart'], $_GET['arrivee'], $_GET['date'], $_GET['prix'], $_GET['conducteurLogin'])) {
            $trajet = self::construireDepuisFormulaire($_GET);

            $success = (new TrajetRepository())->ajouter($trajet);

            if ($success) {
                $trajets = (new TrajetRepository())->recuperer();
                self::afficherVue('vueGenerale.php', [
                    'titre' => 'Trajet créé avec succès',
                    'cheminCorpsVue' => 'trajet/trajetCree.php',
                    'trajets' => $trajets
                ]);
            } else {
                self::afficherErreur('Erreur lors de la création du trajet.');
            }
        } else {
            self::afficherErreur('Tous les champs sont requis.');
        }
    }
    public static function afficherFormulaireMiseAJour(): void
    {
        if (!isset($_GET['id'])) {
            self::afficherErreur("Le id du trajet n'a pas été fourni");
            return;
        }

        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);

        if (!$trajet) {
            self::afficherErreur("Le trajet demandé n'existe pas.");
            return;
        }

        self::afficherVue('vueGenerale.php', [
            'titre' => 'Formulaire de mise à jour trajet',
            'cheminCorpsVue' => 'trajet/formulaireMiseAJour.php',
            'trajet' => $trajet
        ]);
    }
    /*
        public static function mettreAJour(): void
        {
            if (isset($_GET['login'], $_GET['nom'], $_GET['prenom'])) {
                $login = $_GET['login'];
                $nom = $_GET['nom'];
                $prenom = $_GET['prenom'];

                $trajet = new Trajet($login, $nom, $prenom);
                TrajetRepository::mettreAJour($trajet);

                self::afficherVue('vueGenerale.php', [
                    'titre' => 'Trajet mis à jour avec succès',
                    'cheminCorpsVue' => 'trajet/trajetMisAJour.php',
                    'login' => $login,
                    'trajet' => $trajet
                ]);
            } else {
                self::afficherErreur('Tous les champs sont requis pour la mise à jour.');
            }
        }*/
    /**
     *
     * @throws DateMalformedStringException
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire['id'] ?? null;
        $depart = $_GET['depart'];
        $arrivee = $_GET['arrivee'];
        $dateString = $_GET['date'];
        $prix = $_GET['prix'];
        $conducteurLogin = $_GET['conducteurLogin'];
        $nonFumeur = isset($_GET['nonFumeur']) ? 1 : 0;

        $date = new DateTime($dateString);
        $conducteur = (new UtilisateurRepository())->recupererParClePrimaire($conducteurLogin);
        if (!$conducteur) {
            self::afficherErreur("Il n'y a pas d'utilisateur avec le login : " . htmlspecialchars($conducteurLogin));
            exit;
        }
        /** @var Utilisateur $conducteur */
        return new Trajet(null, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);
    }

    /**
     * @throws DateMalformedStringException
     */
    public static function mettreAJour(): void
    {
        if (isset($_GET['id'], $_GET['depart'], $_GET['arrivee'], $_GET['date'], $_GET['prix'], $_GET['conducteurLogin'])) {
            $trajet = self::construireDepuisFormulaire($_GET);
            $trajet->setId($_GET['id']);
            (new TrajetRepository())->mettreAJour($trajet);

            self::afficherVue('vueGenerale.php', [
                'titre' => 'Trajet mis à jour avec succès',
                'cheminCorpsVue' => 'trajet/trajetMisAJour.php',
                'id' => $trajet->getId(),
                'trajet' => $trajet
            ]);
        } else {
            self::afficherErreur('Tous les champs sont requis pour la mise à jour.');
        }
    }
}

?>