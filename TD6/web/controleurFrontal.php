<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

$controleur = 'utilisateur';
if (isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
}

$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);

if (class_exists($nomDeClasseControleur)) {
    $listeFonction = get_class_methods($nomDeClasseControleur);
    if ($listeFonction != null && in_array($_GET["action"], $listeFonction)) {
        $action = $_GET["action"];
        $nomDeClasseControleur::$action();
    }
} else {
    ControleurUtilisateur::afficherErreur("Cette controleur n'existe pas.");
}