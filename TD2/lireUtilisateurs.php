<?php

use Modele\Utilisateur;

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

try {
    $utilisateurs = Utilisateur::recupererUtilisateurs();

    foreach ($utilisateurs as $utilisateur) {
        echo $utilisateur . "<br>";
    }
} catch (Exception $e) {
    echo 'Erreur : ' . $e->getMessage();
}
