<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
        $utilisateurs = [
            ['nom' => 'NGUYEN', 'prenom' => 'Hong Hoa', 'login' => 'nguyenh'],
            ['nom' => 'DUPONT', 'prenom' => 'Jean', 'login' => 'dupontj'],
            ['nom' => 'LEBLANC', 'prenom' => 'Marie', 'login' => 'leblancm']
        ];

        // Vérification avec var_dump pour débogage
        var_dump($utilisateurs);

        if (!empty($utilisateurs)) {
            echo "<h2>Liste des utilisateurs :</h2>";
            echo "<ul>";
            foreach ($utilisateurs as $utilisateur) {
                echo "<li>ModeleUtilisateur {$utilisateur['prenom']} {$utilisateur['nom']} de login {$utilisateur['login']}</li>";
            }
            echo "</ul>";
        } else {
            echo "Il n’y a aucun utilisateur.";
        }
        ?>


    </body>
</html> 