<?php

use Modele\Utilisateur;

require_once "Utilisateur.php";

// Vérifiez que le formulaire a été soumis
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST)) {
    // Récupération des données envoyées par le formulaire
    $login = $_POST['login'];
    $prenom = $_POST['prenom'];
    $nom = $_POST['nom'];

    // Créer une nouvelle instance de ModeleUtilisateur
    $utilisateur = new Utilisateur($login, $nom, $prenom);

    // Afficher l'utilisateur
    echo $utilisateur;
} else {
    echo "Aucune information reçue.";
}
?>
