<?php

use Modele\ConnexionBaseDeDonnees;

require_once "ConnexionBaseDeDonnees.php";

try {
    $pdo = ConnexionBaseDeDonnees::getPdo();
    echo $pdo->getAttribute(PDO::ATTR_CONNECTION_STATUS);
} catch (Exception $e) {
    echo 'Erreur : ' . $e->getMessage();
}
?>
