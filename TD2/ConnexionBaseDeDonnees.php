<?php
require_once 'ConfigurationBaseDeDonnees.php';

class ConnexionBaseDeDonnees {
    private static ?Modele\ConnexionBaseDeDonnees $instance = null;
    private PDO $pdo;

    public static function getPdo(): PDO {
        return self::getInstance()->pdo;
    }

    private static function getInstance(): Modele\ConnexionBaseDeDonnees {
        if (is_null(self::$instance)) {
            self::$instance = new Modele\ConnexionBaseDeDonnees();
        }
        return self::$instance;
    }

    private function __construct() {
        $nomHote = ConfigurationBaseDeDonnees::getNomHote();
        $port = ConfigurationBaseDeDonnees::getPort();
        $nomBaseDeDonnees = ConfigurationBaseDeDonnees::getNomBaseDeDonnees();
        $login = ConfigurationBaseDeDonnees::getLogin();
        $motDePasse = ConfigurationBaseDeDonnees::getMotDePasse();

        try {
            // Connexion à la base de données
            // Le dernier argument sert à ce que toutes les chaines de caractères
            // en entrée et sortie de MySql soient dans le codage UTF-8
            $this->pdo = new PDO("mysql:host=$nomHote;port=$port;dbname=$nomBaseDeDonnees", $login, $motDePasse,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

            // On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('Erreur de connexion à la base de données : ' . $e->getMessage());
        }
    }
}
?>
