<?php

use Modele\ConnexionBaseDeDonnees;

class Utilisateur {
    private $login;
    private $nom;
    private $prenom;

    // Constructeur
    public function __construct($login, $nom, $prenom) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Méthode pour construire un ModeleUtilisateur depuis un tableau SQL
    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Modele\Utilisateur {
        return new Modele\Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    // Pour convertir l'objet en chaîne de caractères
    public function __toString() {
        return "ModeleUtilisateur: $this->prenom $this->nom, login: $this->login";
    }

    // Récupère tous les utilisateurs
    public static function recupererUtilisateurs(): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM utilisateur");
        $utilisateurs = [];
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = Modele\Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }
}

?>
