<?php
// On inclut les fichiers de classe PHP pour pouvoir se servir de la classe ConfigurationBaseDeDonnees.
// require_once évite que ConfigurationBaseDeDonnees.php soit inclus plusieurs fois,
// et donc que la classe ConfigurationBaseDeDonnees soit déclaré plus d'une fois.
require_once 'ConfigurationBaseDeDonnees.php';

// Affichage des informations de configuration pour tester les méthodes
echo 'Login: ' . ConfigurationBaseDeDonnees::getLogin() . "<br>";
echo 'Nom Hôte: ' . ConfigurationBaseDeDonnees::getNomHote() . "<br>";
echo 'Port: ' . ConfigurationBaseDeDonnees::getPort() . "<br>";
echo 'Nom Base De Données: ' . ConfigurationBaseDeDonnees::getNomBaseDeDonnees() . "<br>";
echo 'Mot de Passe: ' . ConfigurationBaseDeDonnees::getMotDePasse() . "<br>";
?>

