<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur;
use TypeError;

class ControleurUtilisateur
{
    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        try {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
            if ($utilisateur == NULL) {
                self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
            } else {
                self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détail de {$utilisateur->getPrenom()} {$utilisateur->getNom()}", "cheminCorpsVue" => "utilisateur/detail.php"]);
            }
        } catch (TypeError $e) {
            self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création d'utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        if (isset($_GET["login"], $_GET["nom"], $_GET["prenom"])) {
            $user = new ModeleUtilisateur($_GET["login"], $_GET["nom"], $_GET["prenom"]);
            $user->ajouter();
            $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
            self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Création d'utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        }
        else {
            self::afficherVue('utilisateur/erreur.php', ["message" => "Les données utilisateur sont manquantes."]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

}