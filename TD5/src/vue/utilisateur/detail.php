<?php
/** @var ModeleUtilisateur $utilisateur */

use App\Covoiturage\Modele\ModeleUtilisateur;

echo "Le login de l'utilisateur " . htmlspecialchars($utilisateur->getPrenom()) . " " . htmlspecialchars($utilisateur->getNom()) . " est " . htmlspecialchars($utilisateur->getLogin()) . ".";