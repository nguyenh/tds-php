<?php

require_once __DIR__ . ('../Modele/ModeleUtilisateur.php');

// Vérifiez que le formulaire a été soumis en POST
if ($_SERVER['REQUEST_METHOD'] === 'GET' && !empty($_GET)) {
    // Récupération des données envoyées par le formulaire
    $login = $_GET['login'];
    $prenom = $_GET['prenom'];
    $nom = $_GET['nom'];

    // Créer une nouvelle instance de ModeleUtilisateur
    $utilisateur = new Utilisateur($login, $nom, $prenom);

    // Enregistrer l'utilisateur dans la base de données
    try {
        $utilisateur->ajouter();
        echo "ModeleUtilisateur ajouté avec succès : " . $utilisateur;
    } catch (Exception $e) {
        echo "Erreur lors de l'ajout de l'utilisateur : " . $e->getMessage();
    }
} else {
    echo "Aucune information reçue.";
}
?>
