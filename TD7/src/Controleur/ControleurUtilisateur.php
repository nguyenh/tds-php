<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Cookie;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\HTTP\Session;

// chargement du modèle
class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs"=>$utilisateurs,"titre" => "Liste des utilisateurs","cheminCorpsVue"=>"utilisateur/liste.php"]);
    }


    public static function afficherDetail(): void
    {
        $login = $_GET["login"];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
        if (empty($login)) {

            ControleurUtilisateur::afficherErreur("Il n'y a aucun login");

        } else {



            if (empty($utilisateur)) {

                ControleurUtilisateur::afficherErreur("L'utilisateur n'existe pas");
            } else {
                ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateur"=>$utilisateur,"titre" => "Détail de l'utilisateur","cheminCorpsVue"=>"utilisateur/detail.php"]);
            }
        }


    }


    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création d'utilisateur","cheminCorpsVue"=>"utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {


        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository)->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository)->recuperer();

        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs"=>$utilisateurs,"titre" => "Utilisateur créé","cheminCorpsVue"=>"utilisateur/utilisateurCree.php"]);;


    }

    public static function afficherErreur(string $messageErreur=" "){

        ControleurUtilisateur::afficherVue('vueGenerale.php',["titre"=>"Erreur","messageErreur"=>$messageErreur,"cheminCorpsVue"=>"utilisateur/erreur.php"]);
    }



    public static function supprimer(){

        $login=$_GET["login"];


        if (empty($login)) {
            ControleurUtilisateur::afficherErreur("Cet utilisateur n'existe pas ");
        }
        else{

            (new UtilisateurRepository())->supprimer($login);
            $utilisateurs = (new UtilisateurRepository())->recuperer();

            ControleurUtilisateur::afficherVue("vueGenerale.php",["titre"=>"Utilisateur supprime","utilisateurs"=>$utilisateurs,"cheminCorpsVue"=>"utilisateur/utilisateurSupprime.php"]);

        }

    }


    public static function afficherFormulaireMiseAJour(){
        $login=$_GET["login"];
        $utilisateur=(new UtilisateurRepository())->recupererParClePrimaire($login);
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateur"=>$utilisateur,"titre" => "Mise à jour d'un utilisateur","cheminCorpsVue"=>"utilisateur/formulaireMiseAJour.php"]);

    }

    public static function mettreAJour(){
        $utilisateur=new Utilisateur($_GET["login"],$_GET["nom"],$_GET["prenom"]);
        (new UtilisateurRepository)->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "login" => $utilisateur->getLogin(), "titre" => "Mise à jour  d'utilisateur effectuée", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }




    /**
     * @param array $tableauDonneesFormulaire
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        foreach ($_GET as $key => $value) {
            $tab[] = $value;
        }
        $utilisateur = new Utilisateur($tab[2], $tab[3], $tab[4]);
        return $utilisateur;
    }

    public static function testerSession(): void
    {
        try {
            // Démarrer la session
            $session = Session::getInstance();
            echo "Session démarrée avec succès.<br>";

            // 1. Enregistrer une chaîne de caractères
            $session->enregistrer("utilisateur", "Cathy Penneflamme");
            echo "Utilisateur enregistré : " . $session->lire("utilisateur") . "<br>";

            // 2. Enregistrer un tableau
            $tableau = ["role" => "admin", "email" => "cathy@example.com"];
            $session->enregistrer("profil", $tableau);
            echo "Profil enregistré : ";
            print_r($session->lire("profil"));
            echo "<br>";

            // 3. Enregistrer un objet
            $utilisateur = new \stdClass();
            $utilisateur->nom = "Penneflamme";
            $utilisateur->prenom = "Cathy";
            $utilisateur->email = "cathy@example.com";
            $session->enregistrer("objetUtilisateur", $utilisateur);
            echo "Objet utilisateur enregistré : ";
            print_r($session->lire("objetUtilisateur"));
            echo "<br>";

            // 4. Supprimer une variable de session
            $session->supprimer("profil");
            echo "Profil supprimé.<br>";

            // Vérifier si la variable a été supprimée
            if (!$session->contient("profil")) {
                echo "La variable 'profil' n'existe plus dans la session.<br>";
            }

            // 5. Détruire complètement la session
            $session->detruire();
            echo "Session détruite et cookie de session supprimé.<br>";
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }



    /*public static function deposerCookie(){

         Cookie::enregistrer($_GET["cle"],$_GET["valeur"],$_GET["dureeExpiration"]);
    }

    public static function lireCookie($cle){
        return Cookie::lire($cle);
    }*/


}

