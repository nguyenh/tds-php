<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{


    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherErreur(string $messageErreur=" "){

        echo ($messageErreur);
    }

    public static function afficherFormulairePreference(){

        ControleurGenerique::afficherVue('vueGenerale.php',["titre"=>"Formulaire Préférence","cheminCorpsVue"=>"formulairePreference.php"]);
    }

    public static function enregistrerPreference(){
        $preference=$_GET['controleur_defaut'];
        PreferenceControleur::enregistrer($preference);
    }
}