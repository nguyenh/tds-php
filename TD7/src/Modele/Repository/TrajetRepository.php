<?php

namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;
use PDO;


class TrajetRepository extends AbstractRepository
{

    protected function construireDepuisTableauSQL(array $objetFormatTableau) : Trajet {

        $leTrajet =new Trajet(
            $objetFormatTableau["id"],
            $objetFormatTableau["depart"],
            $objetFormatTableau["arrivee"],
            new DateTime($objetFormatTableau["date"]),
            $objetFormatTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($objetFormatTableau["conducteurLogin"]),
            $objetFormatTableau["nonFumeur"]
        );
        $leTrajet->setPassagers(TrajetRepository::recupererPassagers($leTrajet));
        return $leTrajet;
    }

    /*public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }*/

    private static function recupererPassagers(Trajet $trajet) :array{
        $sql='SELECT * FROM utilisateur 
              JOIN passager ON passager.passagerLogin = utilisateur.login
              WHERE utilisateur.login IN (SELECT passagerLogin FROM passager
                                                      WHERE trajetId = :trajetId)';

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values=array(
            "trajetId" => $trajet->getId()
        );
        $pdoStatement->execute($values);


        $listepassagers = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);



        return $listepassagers;

    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        if($trajet->isNonFumeur()){
            $nf=1;
        }else{
            $nf=0;
        }

        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag"=>$nf

        );
    }


}