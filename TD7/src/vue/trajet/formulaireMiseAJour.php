<?php
use App\GenerateurAvis\Modele\DataObject\Trajet;
/** @var Trajet $trajet */

?>
<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="controleur" value="trajet"/>
    <input type="hidden" name="action" value="mettreAJour"/>

    <fieldset>
        <legend>Modification de trajet :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="id_id">id&#42;</label>
            <input class="InputAddOn-field" type="text"  name="id" id=id_id" value="<?= htmlspecialchars($trajet->getId())?>" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
            <input class="InputAddOn-field" type="text"  name="depart" id="depart_id" value="<?= htmlspecialchars($trajet->getDepart())?>" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivee&#42;</label>
            <input class="InputAddOn-field" type="text"  name="arrivee" id="arrivee_id" value="<?= htmlspecialchars($trajet->getArrivee())?>" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date"  name="date" id="date_id" value="<?= htmlspecialchars($trajet->getDate()->format("Y-m-d"))?>" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="text"  name="prix" id="prix_id" value="<?= htmlspecialchars($trajet->getPrix())?>" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteur_id">Conducteur&#42;</label>
            <input class="InputAddOn-field" type="text"  name="conducteur" id="conducteur_id" value="<?= htmlspecialchars($trajet->getConducteur()->getLogin())?>" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non Fumeur&#42;</label>
            <input class="InputAddOn-field" type="checkbox"  name="NonFumeur" id="nonFumeur_id" checked >
        </p>



        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>

    </fieldset>
</form>