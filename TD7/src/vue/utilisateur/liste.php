<?php

use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode(($utilisateur->getLogin()));
    echo '<p> Utilisateur de login <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL . '">' . $loginHTML . '</a> &ensp;  (<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Modifier </a>&ensp;<a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL . '">Supprimer ?</a>)</p>';
}
echo '<p> <a href="http://localhost/tds-php/TD6/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation"> Creer un utilisateur</a></p>';
?>
