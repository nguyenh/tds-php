<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur $utilisateur */
echo "Le login de l'utilisateur {$utilisateur->getPrenom()} {$utilisateur->getNom()} est {$utilisateur->getLogin()}.";
?>
</body>
</html>
